<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/contoh', function(){
	return "selamat datang di laravel";
});

//dengan parameter
Route::get('/parameter/{i}', function($i){
	return "halo $i";
});

Route::get('/test', function(){
	return view('test');
});

//dengan parameter
Route::get('/test2/{angka}', function($angka){
	return view('test', ["angka"=>$angka]);
});

//penulisan controller
Route::get('/form','RegisterController@form');
Route::get('/sapa','RegisterController@sapa');
Route::post('/sapa','RegisterController@sapa_post');
Route::get('/','HomeController@home');
Route::get('/register','AuthController@register');
Route::post('/welcome','AuthController@tangkap');